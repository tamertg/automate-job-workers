import "reflect-metadata"

import {
  TheRegentsOfTheUniversityOfCaliforniaSanFranciscoSearch,
} from './worker'

import {
  firefox,
} from 'playwright'

import {
    function1,
    function2,
  function3,
} from 'source'

async function main() {
  const browser = await firefox.launch({
    headless: true,
  })
  const page = await browser.newPage()

  const workerRequest = new function1(
      function3.fromPlain({debug: false}),
    page,
    browser,
    function2.fromPlain({
      taskId: "",
      projectId: "",
      invocationId: "",
      linkIdentifier: "",
      linkType: "",
      workerId: "",
    })
  )

  try {
    const results = await TheRegentsOfTheUniversityOfCaliforniaSanFranciscoSearch(workerRequest)
    console.log(results.data.jobListings)
    console.log(results.data.jobListings.length)
    console.log(results.alerts)
  } catch (error) {
    console.log(error)
    await page.pause()
  }
}

main()
  .catch(console.log.bind(console))
