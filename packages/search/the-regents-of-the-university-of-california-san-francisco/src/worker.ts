
import {
  function1,
  function2,
  function3,
} from '@playwrights'

export async function TheRegentsOfTheUniversityOfCaliforniaSanFranciscoSearch({
                                                                                data,
                                                                                page,
                                                                              }: function5): Promise<function3> {
  const debug = data.debug
  const response = new function3()
  if (debug) {
    response.addAlert({
      type: 'info',
      description: 'Debug mode is enabled',
      status: 'debug',
    })
  }
  const jobList = page.locator('ul.jobList li.baseColorPalette span.ng-scope a:visible')
  const nextButton = page.getByRole('link', {name: 'Next >', disabled: false})

  async function function4() {
    await jobList.first().waitFor()
    await page.waitForTimeout(1000) // to allow page to load and count
    const jobsOnPage = await jobList.count()
    for (let i = 0; i < jobsOnPage; i++) {
      const listing = jobList.nth(i)
      const jobListing = await function2(
        response,
        async () => {
          const title = await function1(
            async () => listing.innerText(),
            'Job Title',
            {
              sanitizeString: true,
            },
          )
          const url = await function1(
            async () => await listing.evaluate((link) => link.href) as string,
            'Job URL',
            {
              sanitizeString: true,
            },
          )
          return {
            title,
            url,
          }
        },
        {
          type: 'Job Listing',
        },
      )
      if (jobListing.alert) {
        continue
      }
      response.addJobListing(jobListing.value)
    }
  }

  if (debug) {
    await page.pause()
  }

  const url = 'https://sjobs.brassring.com/TGnewUI/Search/Home/Home?partnerid=6495&siteid=5861#keyWordSearch=&locationSearch='
  const categories = [
    'advanced practice providers',
    'Nursing',
  ]
  await page.goto(url, {waitUntil: 'networkidle'})
  await page.getByRole('button', {name: 'Search'}).click()
  await page.getByRole('link', {name: 'Filter search results by Job Category '}).click()
  for (let eachCategory of categories) {
    const jobCategorySelector = page.getByRole('textbox', {name: 'Filter Job Category options'})
    await jobCategorySelector.fill(`${eachCategory}`)
    const checkBox = await page.getByRole('checkbox', {name: `${eachCategory}`})
    const checkBoxCount = await checkBox.count()
    for (let i = 0; i < checkBoxCount; i++) {
      checkBox.nth(i).click()
      await page.waitForTimeout(1000) // only way to load and check other box
    }
  }
  while (true) {
    await function4()
    if (debug) {
      await page.pause()
    }
    if (!!await nextButton.count()) {
      const nextUrl = await nextButton.evaluate((link) => link.href) as string
      await Promise.all([
        async () => {
          await nextButton.first().waitFor({state: "detached"})
          await nextButton.first().waitFor({state: "attached"})
        },
        page.waitForURL(nextUrl, { waitUntil: 'networkidle' }),
        nextButton.click(),
      ])
    } else {
      break
    }
  }
  return response
}

